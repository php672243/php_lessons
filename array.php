<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
    //одновимірні масиви
    $nums = array(2,3,4,5,4);
    $nums[0] = 3;
    echo $nums[0] . '<br>';


    $arr = [1.3,35,3.4,-4,'3', true];
    $arr[4] = 'false';
    echo $arr[4] . '<br>';

    // асоціативні по ключам
    $list = ["age" => 20, "name" => 'Vika', "hobby" => 'web-dev', 7 => 'fav number'];
    $list["name"] = 'Victoria';
    echo $list["name"];
    echo '<br>' . $list["7"];

    // багатовимірні
    $matrix = [[1,2,3],[1,2,'three']];
    $matrix[1][1] = 33;
    echo '<br>' . $matrix[1][2];

?>
</body>
</html>