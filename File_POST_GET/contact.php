
<?php
session_start();
$title = "Контакти";
require "blocks/header.php";
?>
<h1 class="mt-5">$title</h1>
<form action="check_contact.php" method="post">
    <input type="text" name="username" value="<?=$_SESSION['username']?>" placeholder="Введіть імя" class="form-control">
    <input type="email" name="email" value="<?=$_SESSION['email']?>" placeholder="Введіть емейл" class="form-control">
    <input type="text" name="subject" value="<?=$_SESSION['subject']?>" placeholder="Введіть тему" class="form-control">
    <textarea name="message" placeholder="Ваше повідомлення" class="form-control">
        <?=$_SESSION['message']?>
    </textarea>
    <button type="submit" class="btn btn-success">Надіслати</button>//submit для перезавантаження сторынки

</form>
<?php

require "blocks/footer.php";
?>