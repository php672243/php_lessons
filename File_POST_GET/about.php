<!--
деректива require, якщо файла не буде, буде помилка і вест код не буде оброблятися
include кщо файла не буде, код після буде виконуватися
include_once файл який був прописаний підключиться лише 1 раз навіть при повторному використанні
-->

<?php

    $title = "Про нас";
    require_once "blocks/header.php" // оскільки хедер це важливи елемент і краще щоб вилазила помилка,
// якщо ж блоки не сильно важливі використовувати include
?>


<div class="container mt-2">
    <h1 class="mt-5">$title</h1>
    <!-- usually for form use post-->
    <form action="check_get.php" method="get">

        <input type="text" name="username" placeholder="Введіть імя" class="form-control">
        <input type="email" name="email" placeholder="Введіть емейл" class="form-control">
        <input type="password" name="password" placeholder="Введіть пароль" class="form-control">
        <textarea name="message" class="form-control" placeholder="Введіть повідомлення"></textarea>
        <input type="submit" value="Надіслати" class="btn btn-success">

    </form>
</div>


<?php
    require_once "blocks/footer.php";
?>