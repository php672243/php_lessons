<?php
    session_start();

    $user_name = htmlspecialchars(trim($_POST['username']));// видаляє хмль теги а трім забирає зайві пробіли
    $from = htmlspecialchars(trim($_POST['email']));
    $subject = htmlspecialchars(trim($_POST['subject']));
    $message = htmlspecialchars(trim($_POST['message']));

    $_SESSION['username'] = $user_name;
    $_SESSION['email'] = $from;
    $_SESSION['subject'] = $subject;
    $_SESSION['message'] = $message;

    if(strlen($user_name) <=1) {
        $error_username = "Input correct username";
    }