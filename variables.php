<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

echo "Hello world";//оператор echo
echo "<br>\"Hello\" world<br>";

// one line comment
/*
 * many line comment
 */


$num=10;// змінна - комірка в памяті компютера
$num=45; //integer
$number= -0.33; // float
$str = "Змінна: "; //string
$bool = "true"; //bool variables (boolean)
echo $str . $num . "<br>" . $str . $number; //concatenation

$b = 0.5;
$a = "0.5";
echo "<br><br>a+b = " . floatval($a) + $b;// floatval приведення до типу даних  float.
echo "<br>" . intval($a);

?>
</body>
</html>