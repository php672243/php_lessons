<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
$x=6;
// якщо треба перевірити одну змінну на велику к-ть значень
switch ($x){
    case 1:
        echo 'Var: 1';
        break;
    case 6:
        echo 'Var: 6';
        break;
    case 5:
        echo 'Var: 5';
        break;
    default:
        echo 'Default work';
}


// also we can use match method
// в отличие от switch:
// -обрабатывает значение в стиле, больше похожем на тернарный оператор.
// -используется строгое сравнение (===), а не слабое (==).
echo match ($x) {
    1 => 'Var: 1',
    6 => 'Var: 6',
    5 => 'Var: 5',
    default => 'Default work',
};

?>
</body>
</html>