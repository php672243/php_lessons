<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
    $x = 10;
    $y = 20;

    echo $x + $y . "<br>";
    echo $x - $y . "<br>";
    echo $x * $y . "<br>";
    echo $x / $y . "<br>";
    echo $x % $y . "<br>";

    $x += 10;
    echo "x = " . $x . "<br>";

    $x -= 10;
    echo "x = " . $x . "<br>";

    $x /= 10;
    echo "x = " . $x . "<br>";

    $x *= 10;
    echo "x = " . $x . "<br>";



    $x++; // == $x += 1;
    $x--; // == $x -= 1;


    echo M_PI . '<br>';
    echo M_E . '<br>';


    echo abs(-22) . '<br>'; // || absolute value

    echo round(3.5) . '<br>'; // Returns the rounded value of val to specified precision
    echo ceil(3.1) . '<br>'; // Round fractions up
    echo floor(3.7) . '<br>'; // Round fractions down

    echo sin(3) . "<br>" . cos(3) . "<br>";

    echo mt_rand(1, 20) . '<br>'; // Generate a random value via the Mersenne Twister Random Number

    echo min(2,6,1,5,1,6,2,6) . '<br>';
    echo max(2,6,1,5,1,6,2,6) . '<br>';

?>
</body>
</html>