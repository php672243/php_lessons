<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
    $str = "Hello";
    echo "VAR: $str"; // interpolations
    echo 'var: ' . $str . '<br>'; // //concatenation
    echo 'VAR: $str <br>'; // string

    echo "<input type='text'> <br>";
    echo "<input type=\"text\"> <br>";


    $length = strlen($str); // Get string length
    echo $length . '<br>';

    echo trim(" rer rer e ") . '<br>'; // Strip whitespace (or other characters) from the beginning and end of a string

    echo strtolower('HELLO!') . '<br>'; // для латиниці

    /*
    $str = "У Мэри Был Маленький Ягнёнок и Она Его Очень ЛЮБИЛА";
    $str = mb_strtolower($str);// якщо є кирилиця
    echo $str;
    */

    echo strtoupper('hello!') . '<br>';


    echo md5(' hello'); // Calculate the md5 hash of a string(for password)
?>
</body>
</html>