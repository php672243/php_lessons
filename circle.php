<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
    // for
    for($i = 0; $i <= 10; $i++) {
        echo $i . '<br>';
    }

    echo '---------<br>';

    // while
    $i = 1;
    $isHasHouse = true;
    while($i <= 10) {
        echo $i.'<br>';
        $i++;
    }

    echo '---------<br>';

    // do while
    $i = 100;
    do{
        echo $i . '<br>';
    } while ($i<10);

    echo '---------<br>';
    // Оператори в циклі
    for($el = 100; $el > 10; $el /= 2){
        if($el < 15)
            break;
        if ($el % 2 == 0)
            continue;

        echo $el . '<br>';
    }

    echo '---------<br>';

    $list = ["age" => 20, "name" => "vika", "weight" => 67];
     foreach($list as $item => $value){
         echo "Key: $item. Value: $value<br>";
     }

    echo '---------<br>';

     $arr = [3,2,3,4,5];
     foreach($arr as $i => $value){
         echo "Index: $i. Value: $value<br>";
     }
?>
</body>
</html>