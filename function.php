<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
    info("hi world");
    function info($word) { // $word - parameter
        echo "$word<br>";
    }
    info("hello");

    function math($x, $y){
        $res = $x + $y;
        info($res);
    }

    math(1,3);
    math(4,3);

    function math2($x, $y){
        return $x + $y;
    }

    $res1 = math2(1,2);
    info($res1);



    function summary($arr) {
        $summa = 0;
        foreach ($arr as $value){
            $summa += $value;
        }

        return $summa;
    }

    $list = [3,3,7];
    echo summary($list) . '<br>';
    echo summary([1.3,4,2,4]) . '<br>';



    function inform (){
        $x = 0; //локальна видимість
    }
    $x = 10;//глобальна видимість
    inform();
    echo $x;

echo '--------------global--------------';
function infor (){
    global $x ; //щоб звернутись до глоббальної змінної
    echo $x;
}
$x = 7;
infor();
echo '<br>' . $x;


echo '--------------static--------------';
function click(){
    static $count; // щоб змінна зерігала свій стан при кожному виклику функції
    $count++;
    echo $count . '<br>';
}
click();
click();
click();

?>
</body>
</html>